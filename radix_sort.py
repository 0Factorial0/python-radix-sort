def counting_sort(list0, place):
    size = len(list0)
    output = [0] * size
    count = [0] * 10

    #calculate count of numbers
    for i in range(0, size):
        index = list0[i] // place
        count[index % 10] += 1

    #calculate cumulative content
    for i in range(1, 10):
        count[i] += count[i-1]

    #place the numbers in sorted order
    i = size - 1
    while i >= 0:
        index = list0[i] // place
        output[count[index % 10]-1] = list0[i]
        count[index % 10] -= 1
        i -= 1

    for i in range(0, size):
        list0[i] = output[i]

#TO-DO
'''
-Add bucket Sort As An Option
'''
def bucket_sort():
    pass

def radix_sort():
    import random

    #get input
    list0 = []
    list_length = int(input("Input List Length: "))

    #generate numbers
    for i in range(0, list_length):
        list0.append(random.randint(0,10000))

    #print unsorted list
    print("-------------")
    print("Unsorted List:",list0)
    print("-------------")

    #get maximum number
    max_num = max(list0)

    #counting sort to sort numbers by index
    place = 1
    while max_num // place > 0:
        counting_sort(list0, place)
        place *= 10
    
    #print sorted list
    print("Sorted List:",list0)
    print("-------------")

radix_sort()